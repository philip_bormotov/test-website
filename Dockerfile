FROM python:3.8-alpine

RUN apk add --update build-base ca-certificates
RUN apk add --no-cache \
    libuv \
    libjpeg \
    zlib \
    zlib-dev \
    libffi-dev \
    libwebp \
    openjpeg \
    jpeg-dev \
    postgresql-libs \
    postgresql-dev \
    ncurses-dev \
    readline-dev \
    gdal-dev \
    geos-dev \
    wkhtmltopdf \
    xvfb \
    ttf-opensans \
    graphviz \
    gettext \
    bash

RUN mkdir /code
ADD requirements.txt /code/
RUN pip install -r /code/requirements.txt
ADD src /code/src
WORKDIR /code/src
