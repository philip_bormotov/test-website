import pytest
from django.core.management import call_command
from django.contrib.auth import get_user_model


@pytest.fixture
def get_user(db):
   User = get_user_model()
   user = User.objects.get(username='admin')
   return user


@pytest.fixture
def api_client(get_user):
    from rest_framework.test import APIClient
    api_client = APIClient()
    api_client.force_authenticate(user=get_user)
    return api_client


@pytest.fixture(scope='session')
def django_db_setup(django_db_blocker):
    with django_db_blocker.unblock():
        call_command('loaddata', 'fixture.json')