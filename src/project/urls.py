from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url


admin.site.site_header = 'Test'
admin.site.site_title = 'Test'

urlpatterns = [
    path('', include('api.v1.urls')),
    path('admin/', admin.site.urls),
]
