from django.db import models


class Sex(models.IntegerChoices):
    MALE = 0, 'мужской'
    FEMALE = 1, 'женский'


class Language(models.TextChoices):
    RUSSIAN = 'ru', 'русский'
    ENGLISH = 'en', 'английский'


class Timezone(models.TextChoices):
    UTC_14 = 'Etc/GMT-14', 'UTC+14'
    UTC_13 = 'Etc/GMT-13', 'UTC+13'
    UTC_12 = 'Etc/GMT-12', 'UTC+12'
    UTC_11 = 'Etc/GMT-11', 'UTC+11'
    UTC_10 = 'Etc/GMT-10', 'UTC+10'
    UTC_9 = 'Etc/GMT-9', 'UTC+9'
    UTC_8 = 'Etc/GMT-8', 'UTC+8'
    UTC_7 = 'Etc/GMT-7', 'UTC+7'
    UTC_6 = 'Etc/GMT-6', 'UTC+6'
    UTC_5 = 'Etc/GMT-5', 'UTC+5'
    UTC_4 = 'Etc/GMT-4', 'UTC+4'
    UTC_3 = 'Etc/GMT-3', 'UTC+3'
    UTC_2 = 'Etc/GMT-2', 'UTC+2'
    UTC_1 = 'Etc/GMT-1', 'UTC+1'
    UTC = 'Etc/GMT', 'UTC'
    UTC_m1 = 'Etc/GMT+1', 'UTC-1'
    UTC_m2 = 'Etc/GMT+2', 'UTC-2'
    UTC_m3 = 'Etc/GMT+3', 'UTC-3'
    UTC_m4 = 'Etc/GMT+4', 'UTC-4'
    UTC_m5 = 'Etc/GMT+5', 'UTC-5'
    UTC_m6 = 'Etc/GMT+6', 'UTC-6'
    UTC_m7 = 'Etc/GMT+7', 'UTC-7'
    UTC_m8 = 'Etc/GMT+8', 'UTC-8'
    UTC_m9 = 'Etc/GMT+9', 'UTC-9'
    UTC_m10 = 'Etc/GMT+10', 'UTC-10'
    UTC_m11 = 'Etc/GMT+11', 'UTC-11'
    UTC_m12 = 'Etc/GMT+12', 'UTC-12'


class WebsiteStatus(models.TextChoices):
    PLAN = 'plan', 'в планах'
    DEV = 'dev', 'в разработке'
    PUBLISH = 'publish', 'опубликован'


