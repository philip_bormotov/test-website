from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions
from rest_framework.views import exception_handler as base_exception_handler


def exception_handler(exc, context):
    response = base_exception_handler(exc, context)
    if isinstance(exc, Http404):
        exc = exceptions.NotFound()
    elif isinstance(exc, PermissionDenied):
        exc = exceptions.PermissionDenied()

    if response is not None:
        data = {
            'detail': {},
        }
        if isinstance(exc.detail, list):
            data['detail'] = exc.detail
        elif isinstance(exc.detail, dict):
            data['detail'] = exc.detail
        else:
            data['detail'] = exc.detail
        response.data = data
    return response
