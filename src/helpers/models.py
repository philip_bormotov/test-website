import uuid

from django.db import models


class UUIDModel(models.Model):
    id = models.UUIDField('ID', default=uuid.uuid4, primary_key=True, editable=False)

    class Meta:
        abstract = True


class Content(models.Model):
    title = models.CharField(verbose_name='Заголовок', default='', max_length=300)
    counter = models.PositiveIntegerField(verbose_name='Кол-во просмотров', default=0)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title
