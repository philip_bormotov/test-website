# Generated by Django 3.0.2 on 2021-03-15 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='title',
            field=models.CharField(default='', max_length=300, verbose_name='Заголовок'),
        ),
    ]
