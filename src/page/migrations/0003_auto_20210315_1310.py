# Generated by Django 3.0.2 on 2021-03-15 13:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0002_page_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='audio',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='page',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='text',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='video',
            name='created_at',
        ),
    ]
