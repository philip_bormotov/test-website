from django.contrib import admin
from .models import Page, Video, Audio, Text


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    filter_horizontal = ('video', 'audio', 'text')
    search_fields = ('title', )


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter')
    search_fields = ('title',)


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter')
    search_fields = ('title',)


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter')
    search_fields = ('title',)
