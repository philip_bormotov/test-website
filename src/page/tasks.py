from django.db.models import F

from project.celery import app
from page.models import Page


@app.task
def count(page_id):
    page = Page.objects.get(id=page_id)
    page.video.update(counter=F('counter') + 1)
    page.audio.update(counter=F('counter') + 1)
    page.text.update(counter=F('counter') + 1)





