from django.db import models

from helpers.models import UUIDModel, Content


class Video(UUIDModel, Content):
    url_file = models.CharField('Cсылка на видеофайл', default='', max_length=300)
    url_sub = models.CharField('Cсылка на файл субтитров', default='', max_length=300)


class Audio(UUIDModel, Content):
    bit = models.PositiveIntegerField('Битрейт в количестве бит в секунду', default=0)


class Text(UUIDModel, Content):
    text = models.TextField('Текст')


class Page(UUIDModel):
    title = models.CharField(verbose_name='Заголовок', default='', max_length=300)
    video = models.ManyToManyField(Video, verbose_name='Видео', related_name='pages', blank=True)
    audio = models.ManyToManyField(Audio, verbose_name='Аудио', related_name='pages', blank=True)
    text = models.ManyToManyField(Text, verbose_name='Текст', related_name='pages', blank=True)

    class Meta:
        verbose_name = 'Старница'
        verbose_name_plural = 'Старницы'

    def __str__(self):
        return self.title

