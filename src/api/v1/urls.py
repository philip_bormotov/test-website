from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, routers

from .page.views import PageViewSet

router = routers.DefaultRouter()
router.register(r'page', PageViewSet, basename='page')


schema_view = get_schema_view(
    openapi.Info(title='Test API',
                 default_version='v1',
                 description='Routes for project'),
    public=True,
    permission_classes=(permissions.IsAuthenticated,),
)

urlpatterns = router.urls

urlpatterns = [
    path('swagger', schema_view.with_ui('swagger'), name='schema-swagger-ui'),
    path('api/v1/', include((urlpatterns, 'api-root')), name='api-root'),
]
