from rest_framework import serializers

from page.models import Page, Video, Audio, Text


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = '__all__'


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = '__all__'


class PageSerializer(serializers.ModelSerializer):
    detail = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = '__all__'

    def get_detail(self, obj):
        return self.context['url'] + str(obj.id) + '/'


class PageReadSerializer(serializers.ModelSerializer):
    video = VideoSerializer(many=True)
    audio = AudioSerializer(many=True)
    text = TextSerializer(many=True)

    class Meta:
        model = Page
        fields = '__all__'
