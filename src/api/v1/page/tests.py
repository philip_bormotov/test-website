import pytest


@pytest.mark.django_db
def test_get_pages(api_client):
    response = api_client.get('/api/v1/page/', follow=True)
    assert response.status_code == 200
    response = response.json()
    page = response['results'][0]
    keys = ['id', 'detail', 'title', 'video', 'audio', 'text']
    assert all(key in page for key in keys)


@pytest.mark.django_db
def test_get_page(api_client):
    response = api_client.get('/api/v1/page/2f75eb42-21c6-42a8-b21c-d05149849212/', follow=True)
    assert response.status_code == 200
    response = response.json()
    keys = ['id', 'title', 'video', 'audio', 'text']
    assert all(key in response for key in keys)
    keys_video = ['id', 'title', 'counter', 'url_file', 'url_sub']
    assert all(key in response['video'][0] for key in keys_video)
    keys_audio = ['id', 'title', 'counter', 'bit']
    assert all(key in response['audio'][0] for key in keys_audio)
    keys_text = ['id', 'title', 'counter', 'text']
    assert all(key in response['text'][0] for key in keys_text)

