from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet
from page.models import Page
from .serializer import PageSerializer, PageReadSerializer
from page.tasks import count


class PageViewSet(ModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def list(self, request, *args, **kwargs):
        url = request.build_absolute_uri()
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer_class = self.get_serializer_class()
            kwargs['context'] = self.get_serializer_context()
            kwargs['context'].update({'url': url})
            serializer = serializer_class(page, many=True, *args, **kwargs)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = PageReadSerializer(instance)
        count.delay(instance.id)
        return Response(serializer.data)