# Test Django API Project

1. Run project  
   

    docker-compose --project-name test up -d --build

2. Migrate db and load data


    docker-compose --project-name test exec -T app python manage.py migrate
    docker-compose --project-name test exec -T app python manage.py loaddata fixture.json

3. Collect admin static


    docker-compose --project-name test exec -T app python manage.py collectstatic

4. Run tests


    docker-compose --project-name test exec -T app pytest


### API (request example)

    curl -X GET "http://0.0.0.0:8000/api/v1/page/" -H  "accept: application/json"
    curl -X GET "http://0.0.0.0:8000/api/v1/page/2f75eb42-21c6-42a8-b21c-d05149849212/" -H  "accept: application/json"

### Admin

    http://0.0.0.0:8000/admin
    admin
    12345

### Docs (swagger)
    
    http://0.0.0.0:8000/swagger